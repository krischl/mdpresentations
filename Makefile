# .SUFFIXES: .html .md
SRCS := $(wildcard md_documents/*.md)
HTML := $(SRCS:md_documents/%.md=%.html)
vpath %.md md_documents
vpath %.html html

%.html: %.md
	pandoc -t revealjs -s -o html/$@ $< -V revealjs-url=reveal.js

#default: test2.html

build: $(HTML) index.md
	pandoc --standalone -o html/index.html index.md
	cp -R images reveal.js html
