---
author: Dr. Sven Bingert & Steffen Rörtgen
title: Infos zum Workshop "Data Science Made Easy - Hasskommentare mit KI identifizieren"
---

# Generelle Informationen zum Workshop

**Thema**: Hasskommentare im Internet mit KI identifizieren

**Zielgruppe**: Jugendliche ab 8. Klasse

**Dauer**: 90 Minuten - 120 Minuten

**Gruppengröße**: max. 20 Personen

**mindestens erforderliche Vorkenntnisse**:
- Handhabung eines PCs
    - Öffnen und Schließen von Programmen
    - Navigieren zu einer Internetseite im Browser
    - Umgang mit einer Maus oder einem Touchpad

**wünschendwerte Vorkenntnisse**:
- Eine Vorbesprechung des Themas im Gesellschafts-/Politik oder Ethik-Unterricht

**erforderliche Hardware**:
- ein PC oder Surface (kein Tablet oder Smartphone) pro Person / pro 2 Personen
- Installation der Software Orange3 (https://orange.biolab.si/)
- Text-Editor auf dem PC / Tablet


# Relevanz des Themas

Der Workshop soll eine Einführung in die Themen "Big Data", "Data Science" und 
"Künstliche Intelligenz" bieten. Die Begriffe, besonders "Künstliche Intelligenz", 
sind bereits seit einiger Zeit immer wieder in gesellschaftlichen Debatten um 
die Zukunft unserer Gesellschaft präsent, jedoch mit wenig Inhalt gefüllt. Dies 
hängt vor allem mit der Tatsache zusammen, dass der Begriff Intelligenz eine starke 
Konnotation besitzt, die meist nur Menschen zugeschrieben wird, in abgeschwächter 
Form bisweilen auch anderen Lebewesen. Was der Begriff Intelligenz dabei mit einer 
leblosen Maschine zu tun hat, ist vielen Menschen nicht wirklich klar.

Der Begriff "Künstliche Intelligenz" ist im Grunde nämlich irreführend, da es sich
dabei um keine Intelligenz im allgemeinen Sinne handelt, sondern um eine schnelle 
Verarbeitung großer Datenmengen mit Hilfe verschiedener Algorithmen. Dabei kann 
der Computer nur genau die Aufgaben erledigen, die ihm vorher zugeteilt wurden, dies
jedoch in einer immer schnelleren Geschwindigkeit und mit immer größeren Datenmengen, 
was zu Erkenntnissen führen kann, die ohne eine entsprechende Maschine nicht gewonnen
werden könnten.

In diesem Workshop wollen wir Schüler:innen zeigen, wie sie selbst eine Maschine
entsprechend für eine Aufgabe trainieren können. 
Dabei orientieren wir uns an aktuellen gesellschaftlichen Themen, denen die Schüler:innen in ihrem Alltag
begegnen. 
Laut des "D21-Digital-Index 2018 / 2019" nutzen 98% der Schüler:innen das Internet. [^1]
Fast alle unter 30-Jähringen sind nach dem gleichen Index in sozialen Medien aktiv.  [^2]
Dabei kommt es auf sozialen Medien immer wieder zu eskalierenden Diskussionen, die 
unter der vermeintlichen Annoymität im Netz auch zu Beleidigungen oder sogenannten 
Hasskommentaren führen. 
In der Altersgruppe 14-24 Jahre sind laut der EU-Initiative klicksafe.de schon 96%
der Nutzer:innen mit Hasskommentaten in Berührung gekommen.[^3]

[^1]: vgl. S. 13 https://initiatived21.de/app/uploads/2019/01/d21_index2018_2019.pdf
[^2]: vgl. S. 24 https://initiatived21.de/app/uploads/2019/01/d21_index2018_2019.pdf
[^3]: vgl. https://www.klicksafe.de/service/aktuelles/news/detail/hate-speech-im-internet/

Das Thema ist damit also hochrelevant für alle Jugendlichen.
Diese Relevanz nehmen wir als Aufhänger für das Thema des Workshops und wollen mit 
den Schüler:innen eine künstliche Intelligenz trainieren, die Hasskommentare von neutralen
oder positiven Kommentaren unterscheiden kann. 
Damit soll den Schüler:innen gezeigt
werden:
- was eine künstliche Intelligenz ist,
- wie eine Künstliche Intelligenz funktioniert und
- wie sie nützlich eingesetzt werden kann.
    

# Ablauf des Workshops

Die Schüler:innen bekommen mittels eines kurzen Videos eine Einführung in das Thema
"Künstliche Intelligenz" und "Big Data". 
Anschließend werden Anwendungsfälle in verschiedenen Bereichen kurz diskutiert:

- Wettervorhersage
- Börsenhandel
- Shopping/Werbung
- Wahlen / Wählerwerbung
- Social Media

In diesem Workshop soll sich auf das Thema Hasskommentare konzentriert werden.
Dazu werden wir uns zunächst einige Hasskommentare anschauen.
Die SuS sollen diskutieren, woran wir als Menschen einen Hasskommentar erkennen.
Dabei werden sie darauf kommen, dass es bestimmte Worte oder Wortkombinationen sind, 
die einen Hasskommentar ausmachen.
Anhand der Beispielsätze werden sie außerdem merken, dass die Worte in ihrem 
Kontext betrachtet werden müssen.
Einige Worte kommen sowohl in negativen, als auch in neutralen oder positiven
Kommentaren vor.
Diese Worte haben dann demnach nicht so einen hohen Informationsgehalt, wie 
andere Worte, die nur in negativen oder nur in neutral/positiven Kommentaren vorkommen.
Genau so, wie die Schüler:innen lernen mussten, das eine von dem anderen zu unterscheiden
und nun diese Unterschiede benennen können, müssen wir der künstlichen Intelligenz 
nun diese Unterschiede zeigen.

Damit eine künstliche Intelligenz "lernen" kann, müssen wir sie 
also entsprechend trainieren.
Dies tun wir, indem wir ihr entsprechende Hasskommentare zeigen und ebenso
neutrale/positive Kommentare. 
Die Klasse wird in zwei Hälften geteilt und die eine Gruppe darf zu einem bestimmten
Thema Hasskommentare verfassen, die andere Gruppe schreibt neutrale/positive Kommentare.
Die Ergebnisse werden dann in einem Online-Dokument zusammengetragen und 
entsprechen mit "negativ" oder "neutral" gekennzeichnet.
Wir werfen einen kurzen Blick auf die Ergebnisse.

Nun öffnen wir Orange3. Es erfolgt eine kurze Einführung in das Programm.
Mit dem File-Widget lässt sich direkt eine Google-Sheets Tabelle einlesen.
Es ist wichtig, dass darauf geachtet wird, dass bei "Kategorie" Type "categorical"
und bei Role "target" steht.

![Einstellungen für File-Widget](images/ss_twitter_polizei_file.png)

Da wir mit Text arbeiten, wird das File-Widget mit dem Corpus-Widget verbunden.
Nun können wir erst einmal mit einer Word-Cloud den die häufigsten Worte betrachten.
Viele uninformative Worte tauchen dort auf. 
Diese Worte besitzen keinen Informationsgehalt und müssen vorher rausgefiltert werden.
Das tun wir mit "Preprocess Text" und wählen bei "Stopwords" "German" aus.
Damit werden die häufigsten deutschen Worte herausgefiltert.
Wir hängen wieder ein Wordcloud Widget an und sehen, dass nun viele uninformative
Worte herausgefiltert worden.
Anschließend "schnüren" wir unser "Bag of Words", d.h. alle vorkommenden Worte 
werden in einen Sack geworfen und von dem Computer nach ihrem Vorkommen hin analysiert.
Dabei wird jedem Wort ein Wert ein numerischer Wert zugeordnet. 
Hierbei ist es wichtig, dass wir bei der Option "Document Frequency" die Option "IDF"
für "Inversed Document Frequecy" auswählen. Ein Beispiel:

- "Du siehst gut aus für dein Alter"
- "Verpiss Dich, Du alter Sack"

In beiden Sätzen kommt das Wort "alter" vor (Groß-/Kleinschreibung wird nicht beachtet).
Ohne die Option "IDF" würde das Wort "alter" einen Wert von zwei zugewiesen bekommen.
Da das Wort aber sowhl in einem positiven, als auch in einem negativen Kontext
vorkommt, können wir davon ausgehen, dass es nicht so einen hohen Informationsgehalt 
besitzt, wie andere Worte, daher wird mit "IDF" diesem Wort ein **NIEDRIGERER**
Wert zugewiesen.

![Einstellungen für Bag of Words](images/ss_twitter_polizei_bow.png)

Anschließend nutzen wir das Model "Neural Net", um unserer Künstliche Intelligenz 
zu trainieren.

Um zu testen, ob unser Model schon gut performed, denken wir uns nun ein paar 
weitere Kommentare aus, die das Model noch nicht kennt.
Diese binden wir wieder mittels File-Widget ein --> Corpus-widget --> Preprocess Text
und wählen dann das Predictions-Widget. Anschließend verbinden wir noch unser Model,
das Neuronale Netz, mit dem Predictions Widget und können dann die Vorhersagen des
Models für die unbekannten Werte sehen.

![Vorhersagen des trainierten Neuronalen Netzes](images/ss_twitter_polizei_predictions.png)

Der gesamte Workflow sieht wie folgt aus:

![Workflow Twitter-Polizei](images/ss_twitter_polizei_workflow.png)


Im Anschluss erfolgt eine Zusammenfassung / Diskussion des Themas.

# Referenzen
