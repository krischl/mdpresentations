---
author: Dr. Sven Bingert & Steffen Rörtgen
title: Data Science Made Easy
subtitle: Hasskommentare mit KI identifizieren
date: 19.06.2019
theme: simple
slideNumber: true
---

[//]: # (USAGE: pandoc -t revealjs -s -o test.html test.md -V revealjs-url=./reveal.js)

## Lizenzinformationen

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Text" property="dct:title" rel="dct:type">Hasskommentare mit KI identifizieren</span> von <span xmlns:cc="http://creativecommons.org/ns#" property="cc:attributionName">Sven Bingert & Steffen Rörtgen</span> ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Namensnennung 4.0 International Lizenz</a>.

## Herzlich willkommen


## Vorstellungsrunde

Wer bist Du?

Bist Du schon einmal mit Data Science in Berührung gekommen?

![](images/vorstellungsrunde.png){ width=20% }[^vorstellungsrunde]

[^vorstellungsrunde]: https://pixabay.com/images/id-3704067/


::: notes

Habt ihr schonmal im Unterricht darüber gesprochen?
Ethik Unterricht?
Informatik Unterricht?

:::


## Data Science und Big Data kurz erklärt
<iframe width="560" height="315" src="https://www.youtube.com/embed/uH813u7_b0s" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


## Was ist Data Science und wo begegnet sie euch?

:::::::::::::: {.columns}
::: {.column width="20%"}

![](images/weather-forecast.png){ height=300px }[^weather]

:::
::: {.column width="20%"}

![](images/boerse.jpg){ height=300px }[^boerse]


:::
::: {.column width="20%"}

![](images/shopping.jpg){ height=300px }[^shopping]


:::
::: {.column width="20%"}

![](images/wahlen.jpg){ height=300px }[^wahlen]


:::
::: {.column width="20%"}

![](images/social_media.jpg){ height=300px }[^social_media]


:::
::::::::::::::


[^weather]: https://pixabay.com/images/id-146472/
[^boerse]: https://pixabay.com/images/id-1905225/
[^shopping]: https://pixabay.com/images/id-1921658/
[^wahlen]: https://pixabay.com/images/id-450166/
[^social_media]: https://pixabay.com/images/id-292994/


::: notes

Wetter --> Wetterstationsdaten
Boerse --> Sammeln von Börsendaten, aber auch Nachrichten
Shopping --> Amazon Vorschläge, Instagram Werbung
Wahlen --> Wähleranalyse, wann platzieren Politiker wo welche Sätze
Social Media --> Analysieren euch für Werbung/Shoppingempfehlung

:::


## Hasskommentare

::: nonincremental

- Sie Pisser haben doch keine Ahnung was wir denken und wissen
- Ja klar aber von dir kann man das du Vollpfosten!
- du Opfer
- DU **ALTER** PENNER; VERPISS DICH DOCH IN DEIN HÄSSLICHES LOCH!!!

:::


::: incremental

- Du bist aber schön für dein **Alter**

- Wie identifiziert ihr Hasskommentare?
- Wie könnte uns Data Science bei der Identifikation helfen?

:::

::: notes

- Kennt ihr den Begriff "Hasskommenatre"?
- Seid ihr auf sozialen Medien schonmal damit in Berührung gekommen?

- Wenn beispielsweise in den letzten beiden Tweets das Wort "Alter" vorkommt, worauf müssen wir achten? --> Kontext

:::


## Beispiel Tweet

<blockquote class="twitter-tweet" data-lang="de"><p lang="de" dir="ltr">Ich finde politisches Engagement von Schülerinnen und Schülern toll. Von Kindern und Jugendlichen kann man aber nicht erwarten, dass sie bereits alle globalen Zusammenhänge, das technisch Sinnvolle und das ökonomisch Machbare sehen. Das ist eine Sache für Profis. CL</p>&mdash; Christian Lindner (@c_lindner) <a href="https://twitter.com/c_lindner/status/1104683096107114497?ref_src=twsrc%5Etfw">10. März 2019</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Arbeitet in Partnerarbeit

- eine Gruppe schreibt Hasskommentare (Lindner, Du Arschloch!!)
- eine Gruppe schreibt neutrale / positive Kommentare (Du hast aber schöne Haare!)


## Tragt eure Ergebnisse zusammen
- <https://kurzelinks.de/twitter-polizei-negativ>
- <https://kurzelinks.de/twitter-polizei-neutral>


## Und jetzt trainieren wir unseren Hass-Detektor
Schauen wir einmal in den Datensatz:
> - <https://kurzelinks.de/twitter-polizei-data>


> - Und jetzt öffnet Orange3


## Zusammenfassung


## Vielen Dank für eure Aufmerksamkeit
<iframe src="https://giphy.com/embed/xIJLgO6rizUJi" width="480" height="367" frameBorder="0" class="giphy-embed" allowFullScreen></iframe>
