# MD-Presentations

Dieses Projekt beschäftigt sich damit, Präsentationen aus Markdown-Dokumenten mittels eines
GITLAB-CI-PANDOC-MARKDOWN Workflows automatisch generieren zu lassen, die dann
auf einer GitLab Pages Seite automatisch angezeigt werden.

**Hintergrund**:
Es besteht das Problem in der Open Educational Ressources Community
(Präsentations-)Inhalte nachhaltig als Open Educational Ressources zu Verfügung
stellen zu können. Gängige proprietäre Formate, wie
PowerPoint oder Apple Keynote sollen dabei vermieden werden, da sie nicht offen
sind.

Markdown eignet sich auf Grund seiner einfachen Lesbarkeit und vielfältigen
Konvertierungsmöglichkeiten gut für dieses Vorhaben. Im Netz sind viele frei
verfügbare Markdown-Editoren zu finden, die sogar kollaborative Möglichkeiten
anbieten. GitLab bietet mit seinen Continous Integration und Pages Tools die Möglichkeit,
Markdown-Dokumente automatisch in HTML zu konvertieren. Mit Hilfe von
[reveal.js](https://github.com/hakimel/reveal.js/) können aus den HTML-Dokumenten
direkt Präsentationen erstellt werden. Diese Präsentationen sind dann über eine
GitLab-Page direkt offen und weltweit verfügbar.

GitLab kann als Instanz auf einem eigenen Server betrieben werden.

Die Idee und die Umsetzung Markdown-Dokumente automatisch in reveal.js
Präsentationen zu konvertieren basieren auf den Vorarbeiten von
[Axel Klinger](https://gitlab.com/axel-klinger/oer-slides)
und [Benji Fisher](https://gitlab.com/benjifisher/slide-decks).


# Link zu Präsentationen

https://sroertgen.gitlab.io/mdpresentations/


# Inhalte

Die Inhalte und erläuternde Dokumente finden sich im Ordner "md_documents".

# Tips / Cheatsheet
[GitLab-Markdown Guide](https://about.gitlab.com/handbook/product/technical-writing/markdown-guide/)


## Metadaten erstellen
Es empfielht sich am Beginn eines Dokumentes Metadaten bereitzustellen, die
Aufschluss über die Autor:innen, Titel u.ä. geben. Wenn es aus dem Dokument
eine Präsentation erstellt wird, werden diese Informationen automatisch auf der
erste Folie dargestellt.

```
---
author: Marina Mustermeier
title: Eine Richtig Coole Präsentation
subtitle: Und zwar so richtig, richtig gut
date: 42.24.4224
theme: simple
slideNumber: true
---
```


## Lizeninformationen einfügen

Auf die [Internetseite von Creative Commons](https://creativecommons.org/choose/#metadata)
gehen und dort die entsprechenden Felder ausfüllen. Den generierten Link dann am Anfang der Präsentation einfügen.


## Shortcuts
- "S" öffnet ein neues Fenster für eine Referentenansicht
- "B" pausiert die Präsentation und schaltet den Bildschirm dunkel
- "ESC" öffnet eine Übersicht aller Slides


## "Animationen" in Slides einfügen / Inhalte nacheinander anzeigen

Inhalte sollen nacheinander eingeblendet werden? Kein Problem:

```HTML
::: incremental
- Das hier
- und das hier
- wird alles nacheinander angezeigt
:::

::: nonincremental
- das hier
- und auch das
- wird alles auf einmal angezeigt
:::
```

aber es geht auch so:

```html
> - Das hier wird zuerst eingeblendet
> - Dann wird das hier eingeblendet
```


## Bilder einfügen

Grundsätzlich ist in dem oben verlinkten Markdown Guide bereits beschrieben, wie Bilder eingefügt werden können. An dieser Stelle noch der Hinweis:

**IN GITLAB DÜRFEN NUR SCREENSHOTS UND PUBLIC DOMAIN BILDER HOCHGELADEN WERDEN**

Die Größe der Bilder lässt sich wie folgt anpassen (hier Breite):

```
![Bildüberschrift](path/to/image.png){ width=50% }
```

Auch die Höhe (hier beispielsweise mit pixels anstatt der relativen Prozentangabe):

```
![Bildüberschrift](path/to/image.png){ height=100px }
```


Beim Einfügen von Bildern sollten in der Markdown Datei auch die entsprechenden
Lizeninformationen hinterlegt werden. Ich empfehle nach dem Bild eine
Markdown-Referenz zu hinterlegen. Damit wird automatisch am Ende der Präsentation
ein Abschnitt mit den Referenzen erzeugt. Auch Lizeninformationen könnten dort
hinterlegt werden. In Der Präsentation selbst wir dann automatisch eine Fußnote
eingefügt.

Die Leerzeile zwischen den Zeilen ist wichtig und die Fußnote immer am Ende
eines Slides eingefügt werden, sie muss nicht direkt hinter das Bild:

```
![Bildüberschrift](path/to/image.png){ width=50% }[^name_der_fussnote]

Hier steht noch ein bisschen Inhalt.

- Vielleicht sogar eine kurze
- ganz kurze Liste

[^name_der_fussnote]: Einfügen des Links zum Bild bzw. Hinterlegen der Lizeninformation

```

**Hinweis**

Es empfiehlt sich die Lizeninfomration, bzw. den Link zum Bild, direkt beim
Hochladen des Bildes als commit message zu hinterlegen.


## Inhalte in zwei Spalten
```
:::::::::::::: {.columns}
::: {.column width="50%"}
![](images/vorstellungsrunde.png){ width=50% }
:::
::: {.column width="50%"}
![](images/vorstellungsrunde.png){ width=50% }
:::
::::::::::::::
```


## Vortragsnotizen

Notizen, die nur in der "Speaker"-Ansicht sichtbar sind, lassen sich ebenfalls
leicht einfügen:

Variante 1:
```
::: notes

Hier sind meine Notizen

:::
```


# Things I Changed

- using a debian docker image in CI
- added installation of build-essentials for using make
- now we can use a newer version of pandoc
- --> allows to use other pandoc commands and markdown gets better converted to html
